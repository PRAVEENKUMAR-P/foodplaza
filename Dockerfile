FROM python:3.8 AS builder

ENV PYTHONUNBUFFERED 1
ENV DJANGO_SETTINGS_MODULE myblog.settings


WORKDIR /app

COPY requirements.txt /app/
RUN pip install -r requirements.txt


COPY . .

# Run unit tests 
RUN python manage.py test

#Create Final Image
FROM python:3.8

# Set environment variables
ENV PYTHONUNBUFFERED 1
ENV DJANGO_SETTINGS_MODULE myblog.settings


WORKDIR /app


COPY --from=builder /app /app


EXPOSE 8000


CMD ["python", "manage.py", "runserver", "0.0.0.0:8000"]
