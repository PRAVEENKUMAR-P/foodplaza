from __future__ import unicode_literals
import multiprocessing

chdir = "/var/www/foodplaza"
pythonpath = "/var/www/foodplaza"
user = "www-data"
group = "www-data"

bind = "127.0.0.1:8001"
workers = multiprocessing.cpu_count() + 1
loglevel = "debug"
proc_name = "myblog"
timeout = 7200
keepalive = 7200
errorlog = "/var/log/foodplaza-error.log"
accesslog = "/var/log/foodplaza-access.log"
